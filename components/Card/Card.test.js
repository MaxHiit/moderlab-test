import '@testing-library/jest-dom'
import { getByText, render } from '@testing-library/vue'
import Card from '~/components/Card/Card.vue'

describe('Card component', () => {
  it('should render a card with header and content', () => {
    const title = 'Test'
    const src = '/some-image.jpg'
    const { container } = render(Card, {
      props: {
        title: title,
        src: src,
      },
      stubs: ['nuxt-img']
    })

    expect(getByText(container, title)).toBeInTheDocument()
    expect(container.querySelector('nuxt-img-stub')).toHaveAttribute('src', src)
  })
})
